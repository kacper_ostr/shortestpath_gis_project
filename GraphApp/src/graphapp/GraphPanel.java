package graphapp;

import edu.uci.ics.jung.algorithms.layout.FRLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;
import edu.uci.ics.jung.graph.util.Pair;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.CrossoverScalingControl;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ScalingControl;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.layout.LayoutTransition;
import edu.uci.ics.jung.visualization.picking.PickedState;
import edu.uci.ics.jung.visualization.util.Animator;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.apache.commons.collections15.Transformer;

/**
 * Klasa renderująca graf w oknie aplikacji w trybie graficznym. 
 * Wykorzystana do tego jest biblioteka JUNG.
 * 
 * @author Kacper Ostrowski
 */
public final class GraphPanel extends JPanel {

    /**
     * deklaracja zmiennych
     */
    ///podstawowa struktura danych reprezentujaca graf
    private final UndirectedSparseMultigraph<Integer, String> r_Graph;
    ///obiekt reprezentujący layout w jakim układany jest graf
    private final Layout<Integer, String> r_layout;
    ///główny obiekt renderujący z biblioteki JUNG
    private final VisualizationViewer<Integer, String> r_mainViewer;
    ///obiekt do kontrolowania skali renderowanego grafu
    private final ScalingControl r_scaler;
    //obiekt do ustawienia trybu obsługi myszy przez panel renderujący
    private final DefaultModalGraphMouse<Integer, String> r_graphMouse;
    //obiekt obsługujący klikanie myszką na wierzchołek grafu
    private PickedState<Integer> r_pickedState;
    //numer wierzchołka końcowego ścieżki
    private Integer r_destinationVertex;
    //lista reprezentująca znalezioną ścieżkę
    private LinkedList<Integer> r_ShortestPath;
    //numer wierzchołka starowego
    private Integer r_sourceVertex;
    //zmienna do logiki zaznaczanie dwóch wierzhołków
    private boolean r_switch;

    public GraphPanel(JFrame parent) {

        //variable to hold destination vertex
        r_destinationVertex = null;
        //variable to hold source vertex
        r_sourceVertex = null;
        //undirected graph init
        r_Graph = new UndirectedSparseMultigraph<>();
        //list which holds verticles from the shortest path
        r_ShortestPath = new LinkedList<>();
        //switch to control selected points
        r_switch = true;

        setBackground(Color.WHITE);
        // show graph
        r_layout = new FRLayout<>(r_Graph);
        r_mainViewer = new VisualizationViewer<>(r_layout);
        r_mainViewer.setBackground(Color.WHITE);

        //setting rules to display vertex and edges. There are used static class to this purpose
        r_mainViewer.getRenderContext().setVertexDrawPaintTransformer(new GraphPanel.VertexPainter<Integer>());
        r_mainViewer.getRenderContext().setVertexFillPaintTransformer(new GraphPanel.VertexFillPainter<Integer>());
        r_mainViewer.getRenderContext().setEdgeDrawPaintTransformer(new GraphPanel.EdgePainter());
        r_mainViewer.getRenderContext().setEdgeStrokeTransformer(new GraphPanel.EdgeStrokePainter());
        r_mainViewer.getRenderContext().setVertexLabelTransformer(new ToStringLabeller<Integer>());

        r_graphMouse = new DefaultModalGraphMouse<>();
        r_mainViewer.setGraphMouse(r_graphMouse);

        r_scaler = new CrossoverScalingControl();

        pickVertexInit();

        setLayout(new BorderLayout());
        add(r_mainViewer, BorderLayout.CENTER);       
    }

    //==========================================================================
    /**
     * funkcja do obsługi zaznaczania wierzchołków i logika zaznaczania 
     * wierzchołka początkowego i końcowego
     */
    private void pickVertexInit() {
        r_pickedState = r_mainViewer.getPickedVertexState();

        // Attach the listener that will print when the vertices selection changes.
        r_pickedState.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                Object subject = e.getItem();
                // The graph uses Integers for vertices.
                if (subject instanceof Integer) {
                    Integer vertex = (Integer) subject;
                    if (r_pickedState.isPicked(vertex)) {
                        
                        if(r_destinationVertex != null && r_sourceVertex == null && Objects.equals(r_destinationVertex, vertex))
                            r_destinationVertex = null;
                        else if(r_destinationVertex != null && r_sourceVertex != null && Objects.equals(r_sourceVertex, vertex))
                            r_sourceVertex = null;
                        else if (r_destinationVertex == null && !Objects.equals(r_sourceVertex, vertex)) {
                            r_destinationVertex = vertex;
                        } else if (Objects.equals(r_destinationVertex, vertex)) {
                            r_destinationVertex = null;
                        } else if (r_sourceVertex == null && !Objects.equals(r_destinationVertex, vertex)) {
                            r_sourceVertex = vertex;
                        } else if (Objects.equals(r_sourceVertex, vertex)) {
                            r_sourceVertex = null;
                        } else if (!Objects.equals(r_destinationVertex, vertex) && r_switch) {
                            r_destinationVertex = vertex;
                            r_switch = false;
                        } else if (!Objects.equals(r_sourceVertex, vertex) && !r_switch) {
                            r_sourceVertex = vertex;
                            r_switch = true;
                        }
                    }
                }
            }
        });
    }
    //==========================================================================
    /**
     * Funkcja do ustawiania layoutu ,na którym ma być wyświetlany graf.
     * @param layoutC klasa jednego z layout'ów z biblioteki JUNG
     */
    public void setCustomLayout(Class<? extends Layout<Integer, String>> layoutC) {
        try {
            //wyłuskiwanie konstruktora i tworzenie nowej instancji layout-u
            Constructor<? extends Layout<Integer, String>> constructor = layoutC
                    .getConstructor(new Class[]{Graph.class});

            Object constructorArgs = r_Graph;

            Object o = constructor.newInstance(constructorArgs);
            Layout<Integer, String> l = (Layout<Integer, String>) o;
            l.setInitializer(r_mainViewer.getGraphLayout());
            l.setSize(r_mainViewer.getSize());

            //ustawienia animacji
            LayoutTransition<Integer, String> lt
                    = new LayoutTransition<>(r_mainViewer, r_mainViewer.getGraphLayout(), l);
            Animator animator = new Animator(lt);
            animator.start();
            r_mainViewer.getRenderContext().getMultiLayerTransformer().setToIdentity();
            r_mainViewer.repaint();

        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }
    //==========================================================================
    /**
     * Funkcja resetująca graf. Czyści kolekcje wierzchołków i krawędzi grafu oraz
     * zmienne służące do trzymania infoemacji o ścieżce
     */
    public void resetGraph() {
 
        Collection<String> tmp_edges = r_Graph.getEdges();
        Collection<Integer> tmp_vertex = r_Graph.getVertices();

        LinkedList<Integer> tmp = null;
        tmp = new LinkedList<>();

        LinkedList<String> tmp2 = null;
        tmp2 = new LinkedList<>();

        for (Integer v : tmp_vertex) {
            tmp.add(v);
        }

        for (Integer v : tmp) {
            r_Graph.removeVertex(v);
        }

        for (String e : tmp_edges) {
            tmp2.add(e);
        }

        for (String e : tmp2) {
            r_Graph.removeEdge(e);
        }

        r_destinationVertex = null;        
        r_sourceVertex = null;
        r_ShortestPath.clear();
        r_mainViewer.getRenderContext().getMultiLayerTransformer().setToIdentity();
        r_mainViewer.repaint();
    }

    //==========================================================================
    /**
     * Funkcja do oddalania lub przybliżania widoku na graf
     *
     * @param zoom 1 powoduje przybliżenie, -1 oddalenie
     */
    public void zoomArea(int zoom) {
        if (zoom == 1) {
            r_scaler.scale(r_mainViewer, 1.1f, r_mainViewer.getCenter());
        } else {
            r_scaler.scale(r_mainViewer, 1 / 1.1f, r_mainViewer.getCenter());
        }
    }

    //==========================================================================
    /**
     * Funkcja ustawiająca tryb uzycia myszy w aplikacji. PICKING to tryb wybierania 
     * wierzchołków, a TRANSFORMING to tryb przesuwania, oddalania i powiększania 
     * całego grafu.
     * @param m dla m == 1 tryb PICKING reszta to TRANSFORMING
     */
    public void setCustomMode(int m) {
        if (m == 1) {
            r_graphMouse.setMode(ModalGraphMouse.Mode.PICKING);
        } else {
            r_graphMouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);
        }
    }
    //============================================================================
    /**
     * Klasa - funktor, obsługująca kolorowanie krawędzi grafu. Dla krawędzi nie 
     * leżącej na ścieżce jest to kolor czarny, a dla leżącej na ścieżce jest to 
     * kolor niebieski.
     */
    public class EdgePainter implements Transformer<String, Paint> {

        public Paint transform(String e) {
            if (r_ShortestPath == null || r_ShortestPath.size() == 0) {
                return Color.BLACK;
            }
            if (isOnThePath(e)) {
                return new Color(0.0f, 0.0f, 1.0f, 0.5f);//Color.BLUE;
            } else {
                return Color.BLACK;
            }
        }
    }

    //============================================================================
    /**
     * Klasa - funktor ustalającja szerokość krawędzi. Dla krawędzi nie należącej 
     * do szerokość jest mała, a dla krawędzi leżącej na ścieżce jest większa.
     */
    public class EdgeStrokePainter implements Transformer<String, Stroke> {

        protected final Stroke THIN = new BasicStroke(1);
        protected final Stroke THICK = new BasicStroke(5);

        public Stroke transform(String e) {
            if (r_ShortestPath == null || r_ShortestPath.size() == 0) {
                return THIN;
            }
            if (isOnThePath(e)) {
                return THICK;
            } else {
                return THIN;
            }
        }
    }
    //============================================================================
    /**
     * Klasa funktor ustalająca kolor obwodu wierzcholków. W tym przypadku
     * jest to kolor czarny.
     */
    public class VertexPainter<V> implements Transformer<V, Paint> {

        public Paint transform(V v) {
            return Color.black;
        }
    }
//============================================================================
    /**
     * Klasa - funktor ustalająca kolor wypełnienia krawędzi. Dla krawędzi nie leżącej
     * na ścieżce jest to kolor szary. Dla wierzchołka leżącego na ścieżce, ale nie
     * będącego wierzchołkiem krańcowym jest to kolor czerwony. Dla wierzchołków 
     * krańcowych jest to kolor niebieski.
     * @param <V> 
     */
    public class VertexFillPainter<V> implements Transformer<V, Paint> {

        public Paint transform(V v) {
            if (v == r_sourceVertex) {
                return Color.BLUE;
            }
            if (v == r_destinationVertex) {
                return Color.BLUE;
            }
            if (r_ShortestPath == null) {
                return Color.LIGHT_GRAY;
            } else {
                if (r_ShortestPath.contains(v)) {
                    return Color.RED;
                } else {
                    return Color.LIGHT_GRAY;
                }
            }
        }
    }
//============================================================================
    /**
     * Funkcja sprawdzjąca czy podana krawędź lezy  na ścieżce.
     * @param e
     * @return 
     */
    private boolean isOnThePath(String e) {

        Pair<Integer> endpoints = r_Graph.getEndpoints(e);
        Integer v1 = endpoints.getFirst();
        Integer v2 = endpoints.getSecond();
        return (v1.equals(v2)) == false && r_ShortestPath.contains(v1) && r_ShortestPath.contains(v2);
    }
//============================================================================

    /**
     * Funkcja wyszukująca najkrótszą ścieżkę i ścieżki awaryjne. 
     * Korzysta z obiektu klasy ShortestPathAlgorithm, w ktorym 
     * zaimplemetowany jest algorytm znajdowania najkrótszej 
     * ścieżki i wszystkich awaryjnych.
     */
    protected void findPaths() {
        if (r_sourceVertex == null || r_destinationVertex == null) {
            return;
        }
        
      ShortestPathAlgorithm spa = new ShortestPathAlgorithm(r_Graph, r_sourceVertex, r_destinationVertex);
      r_ShortestPath.clear();
      LinkedList<Integer> tmp = spa.getShortestOne(null);
      if (!tmp.isEmpty())
    	  r_ShortestPath = tmp;
    }
//==============================================================================
/**
 * Lista zwracająca listę zawierającą najkrótszą ścieżkę i ściezki awaryjne.
 * @return Lista ścieżek awaryjnych i scieżka najkrótsza.
 */
    public LinkedList<LinkedList<Integer>> showShortestPath() {        
        if( r_sourceVertex != null && r_destinationVertex != null )
        {
            findPaths();
            repaint();

            LinkedList<LinkedList<Integer>> allPaths = null;
            allPaths = new LinkedList<>();

            allPaths.add(r_ShortestPath);

            ShortestPathAlgorithm spa;
            for (int i = 1; i < r_ShortestPath.size()-1; ++i)
            {
                    spa = new ShortestPathAlgorithm(r_Graph, r_sourceVertex, r_destinationVertex);
                    allPaths.add(spa.getShortestOne(r_ShortestPath.get(i)));
            }
            return allPaths;
        }
        return null;
    }
    //==========================================================================
    /**
     * Funkcja zwracająca tylko najkrótszą ścieżkę
     * @param pathToShow 
     */
    public void showPath(LinkedList<Integer> pathToShow) {
        if (pathToShow != null) {
            r_ShortestPath.clear();
            r_ShortestPath = pathToShow;

            r_mainViewer.getRenderContext().getMultiLayerTransformer().setToIdentity();
            r_mainViewer.repaint();
        }
    }
    //==========================================================================
    /**
     * Funkcja przyjmująca refrencję na graf i przypisanie jej do zmiennej prywatnej.
     * Ten graf jest wyświetlany na panelu.
     * @param graph 
     */
    public void setGraph(UndirectedSparseMultigraph<Integer, String> graph)
    {
    	resetGraph();
    	for (int i = 0; i < graph.getVertexCount(); ++i)
    		r_Graph.addVertex((Integer) graph.getVertices().toArray()[i]);
    
    	String edge;
    	for (int i = 0; i < graph.getEdgeCount(); ++i)
    	{
    		edge = (String)graph.getEdges().toArray()[i];
    		r_Graph.addEdge(edge, graph.getIncidentVertices(edge));
    	}               
    }
    //==========================================================================
    /**
     * Funkcja odświeżająca widok
     */
    public void paintGraph()
    {                     
        r_mainViewer.getRenderContext().getMultiLayerTransformer().setToIdentity();
        r_mainViewer.repaint();
    }
    /**
     * Funkcja zwracająca referencję na renderowany graf.
     * @return 
     */
    public UndirectedSparseMultigraph<Integer, String> getGraph()
    {
        return r_Graph;
    }
    
    public LinkedList<Integer>  getShortestPath()
    {
            return r_ShortestPath;
    }
}
