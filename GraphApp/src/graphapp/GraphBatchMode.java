/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package graphapp;

import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Klasa służąca do obsługi trybu wsadowego.
 * @author Kacper
 */
public class GraphBatchMode {
    
        //referencja do przetwarzanego obiektu grafu
        private UndirectedSparseMultigraph<Integer, String> r_Graph;
        //wierzchołek początkowy
        Integer r_sourceVertex;
        //wierzchołek końcowy
        Integer r_destinationVertex;
        //lista wierzhołków leżących na najkrótszej ścieżce
        private LinkedList<Integer> r_ShortestPath;
                
        public GraphBatchMode()
        {
            r_Graph = null;
            r_ShortestPath = new LinkedList<>();
        }
        
        /**
         * Funkcja kopiująca podany w argumencie graf.
         * @param graph 
         */
        public void setGraph(UndirectedSparseMultigraph<Integer, String> graph)
        {                        
            if( r_Graph == null)
                r_Graph = new UndirectedSparseMultigraph<>();
            
            resetGraph();
            for (int i = 0; i < graph.getVertexCount(); ++i)
                    r_Graph.addVertex((Integer) graph.getVertices().toArray()[i]);

            String edge;
            for (int i = 0; i < graph.getEdgeCount(); ++i)
            {
                    edge = (String)graph.getEdges().toArray()[i];
                    r_Graph.addEdge(edge, graph.getIncidentVertices(edge));
            }            
        }
        
        /**
         * Funkcja resetujaca graf oraz listę wierzchołków leżacych 
         * na najkrótszej ścieżce i resetowanie wierzchołków
         */
        public void resetGraph() {

        if( r_Graph != null)
        {
            Collection<String> tmp_edges = r_Graph.getEdges();
            Collection<Integer> tmp_vertex = r_Graph.getVertices();

            LinkedList<Integer> tmp = null;
            tmp = new LinkedList<>();

            LinkedList<String> tmp2 = null;
            tmp2 = new LinkedList<>();

            for (Integer v : tmp_vertex) {
                tmp.add(v);
            }

            for (Integer v : tmp) {
                r_Graph.removeVertex(v);
            }

            for (String e : tmp_edges) {
                tmp2.add(e);
            }

            for (String e : tmp2) {
                r_Graph.removeEdge(e);
            }
        }
        
        r_ShortestPath.clear();
        r_sourceVertex = null;
        r_destinationVertex = null;
    }
       
    /**
     * Funkcja przyjmująca numer wierzchołka początkowego i końcowego i zwracająca
     * listę ścieżek awaryjnych i scieżke najkrótszą
     * @param start
     * @param end
     * @return 
     */
    public LinkedList<LinkedList<Integer>> showShortestPath(int start, int end) {

        r_sourceVertex = start;
        r_destinationVertex = end;
            
        findPaths();

        LinkedList<LinkedList<Integer>> allPaths = null;
        allPaths = new LinkedList<>();

        allPaths.add(r_ShortestPath);

        ShortestPathAlgorithm spa;
        for (int i = 1; i < r_ShortestPath.size()-1; ++i)
        {
        	spa = new ShortestPathAlgorithm(r_Graph, r_sourceVertex, r_destinationVertex);
        	allPaths.add(spa.getShortestOne(r_ShortestPath.get(i)));
        }
        return allPaths;
    }
        
    /**
     * Funkcja znajdująca najkrótszą ścieżkę
     */
    protected void findPaths() {
        if (r_sourceVertex == null || r_destinationVertex == null) {
            return;
        }
        
      ShortestPathAlgorithm spa = new ShortestPathAlgorithm(r_Graph, r_sourceVertex, r_destinationVertex);
      r_ShortestPath.clear();
      LinkedList<Integer> tmp = spa.getShortestOne(null);
      if (!tmp.isEmpty())
    	  r_ShortestPath = tmp;
    }
    
    public LinkedList<Integer> getShortestPath()
    {
        return r_ShortestPath;
    }
    
    /**
     * Funkcja zwracająca referencję do grafu przechowywanego w 
     * obiekcie klasy GraphBatchMode
     * @return referencję do grafu
     */
    public UndirectedSparseMultigraph<Integer, String> getGraph()
    {
        return r_Graph;
    }
}
