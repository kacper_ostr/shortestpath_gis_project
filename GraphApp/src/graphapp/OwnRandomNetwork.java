package graphapp;
import java.util.Random;

import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;


public class OwnRandomNetwork {

	private int r_vertexNumber;
	private int r_edgeNumber;
	private UndirectedSparseMultigraph<Integer, String> r_graph;
	
	public OwnRandomNetwork(int vertexNumber, int edgeNumber)
	{
		r_vertexNumber = vertexNumber;
		r_edgeNumber = edgeNumber;
		r_graph = new UndirectedSparseMultigraph<>();
		init();
	}
	
	private void init()
	{
		addVertexes();
		addEdges();
	}
	
	private void addVertexes()
	{
		for (int i = 0; i < r_vertexNumber; ++i)
		{
			r_graph.addVertex(i);
		}
	}
	
	private void addEdges()
	{
		addStartEdges();
		Random generator = new Random();
		int count;
		int source;
		
		for (int i = r_edgeNumber; i < r_vertexNumber; ++i)
		{
			count = 0;
			while (count < r_edgeNumber)
			{
				source = generator.nextInt(i);
				if (source != i && !r_graph.isNeighbor(i, source))
				{
					r_graph.addEdge("Edge"+i+","+source, i, source);
					++count;
				}
			}
		}
	}
	
	private void addStartEdges()
	{
		for (int i = 0; i < r_edgeNumber; ++i)
		{
			for (int j = 0; j < i; ++j)
			{
				r_graph.addEdge("Edge"+i+","+j, i, j);
			}
		}
	}
	
	public UndirectedSparseMultigraph<Integer, String> getGraph()
	{
		return r_graph;
	}
}
