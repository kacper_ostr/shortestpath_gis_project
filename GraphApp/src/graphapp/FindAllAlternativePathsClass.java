package graphapp;

import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;

/**
 * Klasa s�u��ca do znajdowania najkr�tszych alternatywnych �cie�ek z u�yciem rozbicia szukania na w�tki.
 * Jako parametry pszyjmuje graf, wierzcho�ki startowy i docelowy oraz �cie�k� dla kt�rej szukane s� �cie�ki alternatywne.
 *
 */

public class FindAllAlternativePathsClass extends Observable implements Observer{

	private LinkedList<LinkedList<Integer>> paths;
	private UndirectedSparseMultigraph<Integer, String> graph;
	private Integer sourceVertex;
	private Integer destinationVertex;
	private LinkedList<Integer> mainPath;
	private LinkedList<Thread> threads = new LinkedList<>();
	
	public FindAllAlternativePathsClass(UndirectedSparseMultigraph<Integer, String> graph, Integer sourceVertex, Integer destinationVertex, LinkedList<Integer> mainPath)
	{
		this.graph = graph;
		this.destinationVertex = destinationVertex;
		this.sourceVertex = sourceVertex;
		this.mainPath = mainPath;
		paths = new LinkedList<>();
		
		
	}
	
	/**
	 * Metoda rozpoczynaj�ca wyszukiwanie �cie�ek alternatywnych. Ka�da �cie�ka wyszukiwana jest na oddzielnym w�tku.
	 * Metoda oczekuje na zako�czenie wszystkich w�tk�w.
	 */
	public void init()
	{
		for(int i = 1; i < mainPath.size()-1; ++i)
		{
			FindAlternativePathClass find = new FindAlternativePathClass(graph, sourceVertex, destinationVertex, mainPath.get(i));
			find.addObserver(this);
			Thread th = new Thread(find);
			threads.add(th);
			th.start();
		}
		
		for (Thread thread: threads)
		{
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof FindAlternativePathClass)
		{
			paths.add(((FindAlternativePathClass) o).getPath());
			o.deleteObserver(this);
			
			if (paths.size() == mainPath.size()-2);
			{
				this.setChanged();
				notifyObservers();
			}
		}
		
	}
	
	public LinkedList<LinkedList<Integer>> getPaths()
	{
		return paths;
	}
	

}
