package graphapp;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;
import java.util.Random;


public class RandomNetwork {

	private int r_vertexNumber;
	private int r_inversePropapility;
	private UndirectedSparseMultigraph<Integer, String> r_graph;
	
	public RandomNetwork(int vertexNumber, int propability)
	{
		r_vertexNumber = vertexNumber;
		r_inversePropapility = 100 - propability;
		r_graph = new UndirectedSparseMultigraph<>();
		init();
	}
	
	private void init()
	{
		addVertexes();
		addEdges();
	}
	
	private void addVertexes()
	{
		for (int i = 0; i < r_vertexNumber; ++i)
		{
			r_graph.addVertex(i);
		}
	}
	
	private void addEdges()
	{
		Random generator = new Random();
		
		for (int i = 0; i < r_vertexNumber; ++i)
		{
			for (int j = 0; j < i; ++j)
			{
				if (generator.nextInt(100) > r_inversePropapility)
				{
					r_graph.addEdge("Edge"+i+","+j, i, j);
				}
			}
		}
	}
	
	public UndirectedSparseMultigraph<Integer, String> getGraph()
	{
		return r_graph;
	}
}
