package graphapp;
import java.util.LinkedList;
import java.util.concurrent.CopyOnWriteArrayList;

import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;


public class ShortestPathAlgorithm {
	
	private Integer r_source;
	private Integer r_destination;
	private UndirectedSparseMultigraph<Integer, String> r_graph;
	
	public ShortestPathAlgorithm(UndirectedSparseMultigraph<Integer, String> graph, Integer source, Integer destination) {
		r_source = source;
		r_destination = destination;
		r_graph = graph;
	}
	
	public LinkedList<Integer> getShortestOne(Integer without)
	{
		if (r_source == null || r_destination == null || r_graph == null)
			return new LinkedList<>();
		
		Integer prevsTab[] = new Integer[r_graph.getVertexCount()];
		for (int i = 0; i < r_graph.getVertexCount(); ++i)
			prevsTab[i] = null;
		
		int distanceTab[] = new int[r_graph.getVertexCount()];
		for (int i = 0; i < r_graph.getVertexCount(); ++i)
			distanceTab[i] = Integer.MAX_VALUE;
		
		CopyOnWriteArrayList<Integer> vertexToChceck = new CopyOnWriteArrayList<>();
		vertexToChceck.add(r_source);
		distanceTab[r_source] = 0;
		
		Integer vertex;
		
		for (int i = 0; i < vertexToChceck.size(); ++i)
		{
			vertex = vertexToChceck.get(i);
			if (vertex == r_destination)
			{
				continue;
			}
			for (Integer neighbor: r_graph.getNeighbors(vertex))
			{
				if (neighbor == without)
					continue;
				
				int dist = distanceTab[vertex.intValue()] + 1;
				if (distanceTab[neighbor.intValue()] <= dist)
					continue;
				
				if (distanceTab[r_destination.intValue()] <= dist)
					continue;
				
				distanceTab[neighbor.intValue()] = dist;
				prevsTab[neighbor.intValue()] = vertex;
				vertexToChceck.add(neighbor);
			}
		}
		
		LinkedList<Integer> path = new LinkedList<>();
		
		Integer actuall = r_destination;
		while (actuall != r_source)
		{
			if (prevsTab[actuall] == null)
				return new LinkedList<>();
				
			path.addFirst(actuall);
			actuall = prevsTab[actuall];
		}
		path.addFirst(r_source);
		return path;
	}
}
