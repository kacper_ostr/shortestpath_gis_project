/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package graphapp;

import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;
import edu.uci.ics.jung.io.GraphMLReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.collections15.Factory;
import org.xml.sax.SAXException;

/**
 * Klasa do wczytywania grafu z plików csv lub GraphML
 * @author Kacper
 */
public class GraphFileLoader {
    
    
    private final UndirectedSparseMultigraph<Integer, String> r_Graph;
    

    public GraphFileLoader()
    {
        r_Graph = new UndirectedSparseMultigraph<>();       
    }
    
    /**
     * Funkcja przyjmujaca ścieżkę pliku do wczytania. Jeżeli rozszerzenie pliku
     * nie jest zgodne z założonym to zwracana jest wartość false.
     * @param filePath ściezka do pliku z grafem
     * @return boolean czy pli został wczytany
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public boolean loadFile(String filePath) throws ParserConfigurationException, SAXException, IOException {

        if (filePath.substring(filePath.length() - 7, filePath.length()).equals("graphml")) {
            return loadGraphMLFile(filePath);
        } else if (filePath.substring(filePath.length() - 3, filePath.length()).equals("csv")) {
            return loadCSVFile(filePath);
        } 
        return false;
    }
       
    /**
     * Funkcja wczytująca plik z formatu GraphML. Wykorzystuje parser dostarczony 
     * w bibliotece JUNG.
     * @param filePath ściezka do pliku z grafem
     * @return
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    private boolean loadGraphMLFile(String filePath) throws ParserConfigurationException, SAXException, IOException {

        Factory<Integer> vertexFactory = new Factory<Integer>() {
            int n = 0;

            public Integer create() {
                return n++;
            }
        };
        Factory<String> edgeFactory = new Factory<String>() {
            int n = 0;

            String prefix = "Edge_";

            public String create() {
                return prefix + Integer.toString(n++);
            }
        };

        GraphMLReader<UndirectedSparseMultigraph<Integer, String>, Integer, String> gmlr
                = new GraphMLReader<>(vertexFactory, edgeFactory);

        gmlr.load(filePath, r_Graph);

        return true;
    }
    
    /**
     * Funkcja wczytująca graf z pliku csv. Plik jest skonstruowany następująco: 
     * liczba wierzchołków; liczba krawędzi
     * wierzchołek_1
     * wierzchołek_2
     * ...
     * nazwa_krawędzi_1;wierzchołek_src;wierzchołek_dst
     * nazwa_krawędzi_2;wierzchołek_src;wierzchołek_dst 
     * ...
     *
     * ważne, aby wierzchołki były nimerowane od zera
     * 
     * @param filePath ścieżka pliku     
     * @throws FileNotFoundException
     */
    private boolean loadCSVFile(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        Scanner in = new Scanner(file);

        int vertexAmnt,
                edgeAmnt;

        StringTokenizer st = new StringTokenizer(in.nextLine(), ";");
        vertexAmnt = Integer.parseInt(st.nextToken());
        edgeAmnt = Integer.parseInt(st.nextToken());

        for (int i = 0; i < vertexAmnt; i++) {
            r_Graph.addVertex(in.nextInt());
        }

        in.nextLine();
        for (int i = 0; i < edgeAmnt; i++) {
            StringTokenizer st_2 = new StringTokenizer(in.nextLine(), ";");

            String label = st_2.nextToken();

            String tmp_1 = st_2.nextToken();
            int vertex_1 = Integer.parseInt(tmp_1);

            String tmp_2 = st_2.nextToken();
            int vertex_2 = Integer.parseInt(tmp_2);

            r_Graph.addEdge(label, vertex_1, vertex_2);
        }

        return true;
    }
    
    /**
     * Funkcja zwraca referencję do wczytanego grafu z pliku
     * @return 
     */
    public UndirectedSparseMultigraph<Integer, String> getGraph()
    {
        return r_Graph;     
    }
}
