package graphapp;

import com.rits.cloning.Cloner;

import edu.uci.ics.jung.algorithms.shortestpath.BFSDistanceLabeler;
import edu.uci.ics.jung.graph.SparseMultigraph;
import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import static org.junit.Assert.*;
import org.junit.Test;

public class SimpleAlgorithmTest {

    private int max = 100;

	@Test
	public void testCzasWykonaniaKrawedzie() {

		Random r = new Random();
		Integer source = 1;
		Integer dest = 1;
		long start, stop;
		
		try {
			File f = new File("testCzasuWykonaniaKrawedzie.txt");
			if (!f.exists())
				f.createNewFile();
			
			PrintWriter writer = new PrintWriter("testCzasuWykonaniaKrawedzie.txt");
		
			for (int i = 2; i < 22; ++i)
			{
				writer.println(50000 + "," + 5000*i*2 + "\n");
				for (int k = 0; k < 50; ++k)
				{
					OwnRandomNetwork orn = new OwnRandomNetwork(50000, i);
					source = r.nextInt(50000-1)+1;
					do
					{
						dest = r.nextInt(50000-1)+1;
					} while (dest == source);
					
					start = System.currentTimeMillis();
					findPaths(orn.getGraph(), source, dest).size();
					stop = System.currentTimeMillis();
					
					writer.println(stop-start);
				}
				writer.println("");
			}
		writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

    protected LinkedList<Integer> findPaths(UndirectedSparseMultigraph<Integer, String> graph, Integer sourceVertex, Integer destinationVertex) {
        LinkedList<Integer> path = new LinkedList<>();

        ShortestPathAlgorithm spa = new ShortestPathAlgorithm(graph, sourceVertex, destinationVertex);

        LinkedList<Integer> tmp = spa.getShortestOne(null);
        if (!tmp.isEmpty()) {
            path = tmp;
        }

        return path;
    }

    public static void main(String args[]) {

        SimpleAlgorithmTest sat = new SimpleAlgorithmTest();
        sat.testCzasWykonaniaKrawedzie();
    }
}
