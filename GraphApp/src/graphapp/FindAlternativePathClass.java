package graphapp;

import java.util.LinkedList;
import java.util.Observable;

import edu.uci.ics.jung.graph.UndirectedSparseMultigraph;

/**
 * Klasa pozwalajaca na znalezienie jednej ze �cie�ek alternatywnych na oddzielnym w�tku.
 * Jako parametry przyjmuje graf i wierzcho�ki:startowy, docelowy oraz wykluczony z wyszukiwanej �cie�ki.
 *
 */
public class FindAlternativePathClass extends Observable implements Runnable{

	private UndirectedSparseMultigraph<Integer, String> graph;
	private Integer sourceVertex;
	private Integer destinationVertex;
	private Integer vertex;
	private LinkedList<Integer> path;
	
	public FindAlternativePathClass(UndirectedSparseMultigraph<Integer, String> graph, Integer sourceVertex, Integer destinationVertex, Integer vertex)
	{
		this.graph = graph;
		this.sourceVertex = sourceVertex;
		this.destinationVertex = destinationVertex;
		this.vertex = vertex;
	}

	@Override
	public void run() {
		ShortestPathAlgorithm sp = new ShortestPathAlgorithm(graph, sourceVertex, destinationVertex);
		path = sp.getShortestOne(vertex);
		
		this.setChanged();
		notifyObservers();
	}
	
	public LinkedList<Integer> getPath()
	{
		return path;
	}
}
