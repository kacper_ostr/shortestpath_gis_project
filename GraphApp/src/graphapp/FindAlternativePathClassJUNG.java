package graphapp;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Observable;
import java.util.Set;

import com.rits.cloning.Cloner;

import edu.uci.ics.jung.algorithms.shortestpath.BFSDistanceLabeler;
import edu.uci.ics.jung.graph.SparseMultigraph;

public class FindAlternativePathClassJUNG extends Observable implements Runnable{

	private SparseMultigraph<Integer, String> graph;
	private Integer sourceVertex;
	private Integer destinationVertex;
	private Integer vertex;
	private LinkedList<Integer> path;
	
	public FindAlternativePathClassJUNG(SparseMultigraph<Integer, String> graph, Integer sourceVertex, Integer destinationVertex, Integer vertex)
	{
		this.graph = graph;
		this.sourceVertex = sourceVertex;
		this.destinationVertex = destinationVertex;
		this.vertex = vertex;
	}

	@Override
	public void run() {
		
		Cloner cloner = new Cloner();
		SparseMultigraph<Integer, String> graph = cloner.deepClone(this.graph);
		
		graph.removeVertex(vertex);
		
		path = getPathsWithJUNG(graph, sourceVertex, destinationVertex);
		
		this.setChanged();
		notifyObservers();
	}
	
	public LinkedList<Integer> getPath()
	{
		return path;
	}
	
	protected LinkedList<Integer> getPathsWithJUNG(SparseMultigraph<Integer, String> graph, Integer sourceVertex, Integer destinationVertex)
    {
    	LinkedList<Integer> path = new LinkedList<>();
    	LinkedList<Integer> tmp = new LinkedList<>();
    	BFSDistanceLabeler<Integer, String> bdl = new BFSDistanceLabeler<>();
        bdl.labelDistances(graph, sourceVertex);
        tmp.clear();

        Integer v = destinationVertex;
        Set<Integer> prd = bdl.getPredecessors(v);
        tmp.add(destinationVertex);
        while (prd != null && prd.size() > 0) {
            v = prd.iterator().next();
            tmp.add(v);
            if (Objects.equals(v, sourceVertex)) {
            	path = tmp;
                break;
            }
            prd = bdl.getPredecessors(v);
        }
        return path;
    }
}
