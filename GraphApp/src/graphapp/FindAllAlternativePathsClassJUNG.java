package graphapp;

import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import edu.uci.ics.jung.graph.SparseMultigraph;

public class FindAllAlternativePathsClassJUNG extends Observable implements Observer{

	private LinkedList<LinkedList<Integer>> paths;
	private SparseMultigraph<Integer, String> graph;
	private Integer sourceVertex;
	private Integer destinationVertex;
	private LinkedList<Integer> mainPath;
	private LinkedList<Thread> threads = new LinkedList<>();
	
	public FindAllAlternativePathsClassJUNG(SparseMultigraph<Integer, String> graph, Integer sourceVertex, Integer destinationVertex, LinkedList<Integer> mainPath)
	{
		this.graph = graph;
		this.destinationVertex = destinationVertex;
		this.sourceVertex = sourceVertex;
		this.mainPath = mainPath;
		paths = new LinkedList<>();
		
		
	}
	
	public void init()
	{
		for(int i = 1; i < mainPath.size()-1; ++i)
		{
			FindAlternativePathClassJUNG find = new FindAlternativePathClassJUNG(graph, sourceVertex, destinationVertex, mainPath.get(i));
			find.addObserver(this);
			Thread th = new Thread(find);
			threads.add(th);
			th.start();
		}
		
		for (Thread thread: threads)
		{
			try {
				thread.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof FindAlternativePathClass)
		{
			paths.add(((FindAlternativePathClass) o).getPath());
			o.deleteObserver(this);
			
			if (paths.size() == mainPath.size()-2);
			{
				this.setChanged();
				notifyObservers();
			}
		}
		
	}
	
	public LinkedList<LinkedList<Integer>> getPaths()
	{
		return paths;
	}
	

}
